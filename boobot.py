﻿import datetime
import random
import re
import time
from functools import partial

import boobot_frontend as bf
import tgl
from typing import Callable

# Boobot

NOT_FOUND_ANSWER = [
    'Не нашлось такого :(',
    'Чё за хуита?!',
    'Такого здесь не водится'
]

NOT_FOUND_REPLY_ANSWER = [
    'Это какое-то древнее говно',
    'Нет такого. Может потерялось... :(',
]

BORING_MESSAGES = [
    'Скучно тут у вас...',
    'Тихо как-то...',
    'Оживим чатик!'
]

LUNCH_MESSAGES = [
    'Сиськи к обеду!',
    'Обеденные сиськи!',
    'Не ешьте сиськи!'
]


def handle_message(msg: tgl.Msg, peer: tgl.Peer):
    if msg.reply_id:
        if msg.mention:
            handle_reply(msg, peer, msg.reply)
        return
    if msg.text is None:
        return

    handler = get_handler(msg.text)
    if handler is not None:
        handler(msg, peer)
    peer.mark_read(on_empty_cb)


def get_handler(text):
    text = text.lower()
    for key in FULL_TEXT_HANDLERS:
        if re.match(key, text):
            return FULL_TEXT_HANDLERS[key]
    return None


def handle_reply(msg: tgl.Msg, peer: tgl.Peer, reply: tgl.Msg):
    if reply is None:
        find_message_by_id_async(peer, msg.reply_id, partial(handle_reply, msg, peer))
        return

    print('Handle reply to', reply.id, 'from', reply.date)
    img = bf.find_by_id(msg.reply_id)
    if re.match(re_key('ссылк(у|а|и|ой)', 'ссыль'), msg.text):
        peer.send_msg(img.get_url() if img is not None else random.choice(NOT_FOUND_REPLY_ANSWER), reply=msg.id)
    elif re.match(re_key('оригинал[^ ]*', 'сорс'), msg.text):
        peer.send_msg(img.image if img is not None else random.choice(NOT_FOUND_REPLY_ANSWER), reply=msg.id)
    elif re.match(re_key('пост'), msg.text):
        peer.send_msg(img.post if img is not None else random.choice(NOT_FOUND_REPLY_ANSWER), reply=msg.id)


def tag_handler(tag: str, msg: tgl.Msg, peer: tgl.Peer):
    print('Find tag "' + tag + '"')
    img = bf.find_by_tag(tag)
    send_image(img, msg, peer)


def random_handler(msg: tgl.Msg, peer: tgl.Peer):
    img = bf.find_random()
    send_image(img, msg, peer)


def send_image(img: bf.Image, msg: tgl.Msg, peer: tgl.Peer):
    msg_id = msg.id if msg is not None else None
    if img is None:
        peer.send_msg(random.choice(NOT_FOUND_ANSWER), reply=msg_id)
        return

    print('Sending', img.file)
    if img.file.endswith(".gif"):
        peer.send_video(img.file, partial(on_photo_sent, img, peer))
    else:
        peer.send_photo(img.file, partial(on_photo_sent, img, peer))


def on_photo_sent(img: bf.Image, peer: tgl.Peer, success: bool, msg: tgl.Msg):
    if success:
        if msg is None:
            find_last_own_message_async(peer, partial(on_photo_sent, img, peer, True))
            return

        img.set_sent(msg.id)


def on_load_photo_cb(success: bool, file: str):
    print('Message photo', file)


def on_empty_cb(success: bool):
    pass

СИСЬКИ = '(сиськ(и|ам|ами|ах|а|у|е|ой)|сисек)'
СИСЕЧКИ = '(сисечк(и|ам|ами|ах|а|у|е|ой)|сисечек)'
ПОПА = 'поп(а|ы|у|е|ой|ам|ами|ах|)'
ЖОПА = 'жоп(а|ы|у|е|ой|ам|ами|ах|)'


def re_key(*args):
    return '^(|.*[^\w])(' + '|'.join(args) + ')(|[^\w].*)$'

FULL_TEXT_HANDLERS = {
    re_key('!c', '!с', СИСЬКИ, СИСЕЧКИ): partial(tag_handler, 'сиськи'),
    re_key('!п', '!ж', ПОПА, ЖОПА): partial(tag_handler, 'попа'),
    re_key('!ф', 'красивая фигура'): partial(tag_handler, 'красивая фигура'),
    re_key('!r(and(om)?)?', '!р(анд(ом)?)?'): random_handler
}


# Telegram helper

HISTORY_QUERY_LIMIT = 100


def on_history_cb(offset: int, peer: tgl.Peer, predicate, cb, success: bool, msgs: [tgl.Msg]):
    if msgs is not None:
        print('Search in', len(msgs), 'messages...')
        for msg in msgs:
            if predicate(msg):
                print('Found', msg.id, 'from', msg.date)
                cb(msg)
                return
        if len(msgs) != HISTORY_QUERY_LIMIT:
            return
        offset += len(msgs)

    print('Continue search from', offset, 'offset')
    tgl.get_history(peer, offset, HISTORY_QUERY_LIMIT, partial(on_history_cb, offset, peer, predicate, cb))


def find_message(peer: tgl.Peer, predicate: Callable[[tgl.Msg], bool], cb: Callable[[tgl.Msg], None]):
    on_history_cb(0, peer, predicate, cb, True, None)


def find_message_by_id_async(peer: tgl.Peer, id: int, cb: Callable[[tgl.Msg], None]):
    def predicate(m: tgl.Msg):
        return m.id == id

    find_message(peer, predicate, cb)


def find_last_own_message_async(peer: tgl.Peer, cb: Callable[[tgl.Msg], None]):
    def predicate(m: tgl.Msg):
        return m.src.id == our_id

    find_message(peer, predicate, cb)


def boring_handler():
    global last_chat_update
    chat_peer.send_msg(random.choice(BORING_MESSAGES))
    random_handler(None, chat_peer)
    last_chat_update = time.time()


def lunch_handler():
    chat_peer.send_msg(random.choice(LUNCH_MESSAGES))
    tag_handler('сиськи', None, chat_peer)

# Telegram stuff

our_id = 0
binlog_done = False

chat_peer = None
last_chat_update = time.time()
last_lunch_weekday = -1


def on_binlog_replay_end():
    global binlog_done
    binlog_done = True


def on_get_difference_end():
    pass


def on_our_id(id):
    global our_id
    our_id = id
    return "Set ID: " + str(our_id)


def on_msg_receive(msg):
    global chat_peer, last_chat_update
    if msg.out and not binlog_done:
        return

    peer = msg.src if msg.dest.id == our_id else msg.dest
    if chat_peer is None and peer.type == tgl.PEER_CHAT:
        chat_peer = peer
        print('Chat peer is', chat_peer.id)

    if chat_peer is not None:
        last_chat_update = time.time()

    handle_message(msg, peer)


def on_secret_chat_update(peer, changes):
    return "on_secret_chat_update"


def on_user_update(peer, changes):
    dir(changes)
    pass


def on_chat_update(peer, changes):
    dir(changes)
    pass


BORING_INTERVAL = 60 * 60 * 6


def handle_boring_chat():
    delta = time.time() - last_chat_update
    if delta > BORING_INTERVAL:
        boring_handler()


def handle_lunch():
    global last_lunch_weekday
    today = datetime.datetime.today()
    wd = today.weekday()
    if wd != last_lunch_weekday and wd < 5 and today.hour == 14:
        delta = time.time() - last_chat_update
        if delta > 600:
            last_lunch_weekday = wd
            lunch_handler()


def on_loop():
    if chat_peer is not None:
        handle_boring_chat()
        handle_lunch()

    pass


# Set callbacks
tgl.set_on_binlog_replay_end(on_binlog_replay_end)
tgl.set_on_get_difference_end(on_get_difference_end)
tgl.set_on_our_id(on_our_id)
tgl.set_on_msg_receive(on_msg_receive)
tgl.set_on_secret_chat_update(on_secret_chat_update)
tgl.set_on_user_update(on_user_update)
tgl.set_on_chat_update(on_chat_update)
tgl.set_on_loop(on_loop)
