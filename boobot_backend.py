import imghdr
import os
import sqlite3
import struct
import time
import network_helper as nh


LOCAL_PATH = '/var/www/local.wnezros.com/boobot/'
WEB_PATH = 'http://local.wnezros.com/boobot/'

TASK_LIST = [
    {'method': 'joyreactor_downloader.download_tag', 'param': 'сиськи'},
    {'method': 'joyreactor_downloader.download_tag', 'param': 'попа'},
    {'method': 'joyreactor_downloader.download_tag', 'param': 'красивая фигура'}
]


def load_task(task, page):
    method = None
    method_name = task['method']
    idx = method_name.find('.')
    if idx == -1:
        method = locals()[method_name]
    else:
        module = __import__(method_name[:idx])
        method = getattr(module, method_name[idx+1:])

    return method(task['param'], page)


def get_next_page(c, task):
    c.execute('SELECT page FROM parsing WHERE method=? AND param=?', (task['method'], task['param'],))
    row = c.fetchone()
    if row is not None:
        return int(row[0])
    return 1


def set_next_page(c, task, page):
    c.execute('UPDATE parsing SET page=? WHERE method=? AND param=?', (page, task['method'], task['param'],))
    if c.rowcount == 0:
        c.execute('INSERT INTO parsing VALUES(?, ?, ?)', (task['method'], task['param'], page,))


def get_image_size(fname):
    imgtype = imghdr.what(fname)
    if imgtype is None:
        print('Unknown image -> convert')
        os.system('convert -interlace Plane -quality 85% {0} {0}'.format(fname))
        imgtype = imghdr.what(fname)

    with open(fname, 'rb') as fhandle:
        head = fhandle.read(24)
        if len(head) != 24:
            return
        if imgtype == 'png':
            check = struct.unpack('>i', head[4:8])[0]
            if check != 0x0d0a1a0a:
                return
            width, height = struct.unpack('>ii', head[16:24])
        elif imgtype == 'gif':
            width, height = struct.unpack('<HH', head[6:10])
        elif imgtype == 'jpeg':
            try:
                fhandle.seek(0) # Read 0xff next
                size = 2
                ftype = 0
                while not 0xc0 <= ftype <= 0xcf:
                    fhandle.seek(size, 1)
                    byte = fhandle.read(1)
                    while ord(byte) == 0xff:
                        byte = fhandle.read(1)
                    ftype = ord(byte)
                    size = struct.unpack('>H', fhandle.read(2))[0] - 2
                # We are at a SOFn block
                fhandle.seek(1, 1)  # Skip `precision' byte.
                height, width = struct.unpack('>HH', fhandle.read(4))
            except Exception:  # IGNORE:W0703
                return
        else:
            return
    return width, height


def load_image(url, post, base_name):
    file = LOCAL_PATH + base_name
    file = nh.download_binary(url, file, referer=post)
    w, h = get_image_size(file)
    return file, w, h


def perform_task(task, c):
    page = get_next_page(c, task)
    data = load_task(task, page)

    posts = [post['post'] for post in data['posts']]
    seq = ','.join(['?'] * len(posts))
    c.execute('SELECT post FROM data WHERE post IN (%s)' % seq, posts)
    for row in c.fetchall():
        if row[0] in posts:
            posts.remove(row[0])

    sql_data = []
    for post in data['posts']:
        if post['post'] not in posts:
            continue

        tags = '~' + '~'.join(post['tags']) + '~'
        for image in post['images']:
            sql_data.append((post['post'], image, None, tags, None, 0, 0, None))

    if len(sql_data) > 0:
        c.executemany('INSERT INTO data VALUES(?,?,?,?,?,?,?,?)', sql_data)

    if 'next' in data:
        set_next_page(c, task, data['next'])
        return True
    return False


def get_top_domain(domain: str) -> str:
    parts = domain.split(':')[0].split('.')
    return '.'.join(parts[-2:])


def get_path_id(path: str) -> str:
    path = path[(path.rfind('/') + 1):]
    dot = path.rfind('.')
    if dot != -1:
        path = path[0:dot]

    max_id = 0
    for p in path.split('-'):
        if p.isdigit():
            num = int(p)
            max_id = max(num, max_id)

    if max_id == 0:
        max_id = abs(hash(path))

    return str(max_id)


def load_images(c, limit=10):
    import urllib.parse
    import urllib.error

    c.execute('SELECT image,post FROM data WHERE file IS NULL LIMIT ' + str(limit))
    result = c.fetchall()
    index = 1
    for row in result:
        image, post = row
        img_url = urllib.parse.urlsplit(image)
        domain = get_top_domain(img_url.netloc)
        path = get_path_id(img_url.path)
        base_name = domain[0:3] + '_' + path
        print('Load image', index, base_name)
        index += 1

        try:
            file, w, h = load_image(image, post, base_name)
            c.execute('UPDATE data SET file=?,width=?,height=? WHERE image=?', (file, w, h, image,))
        except urllib.error.HTTPError as err:
            if err.code == 404:
                print('Delete image', image)
                c.execute('DELETE FROM data WHERE image=?', (image,))

        time.sleep(1)
    return len(result)


def perform_tasks():
    db = sqlite3.connect('db-work.db')
    c = db.cursor()

    for task in TASK_LIST:
        print(task['method'], task['param'])
        while perform_task(task, c):
            db.commit()
            time.sleep(3)

    print('Load images...')
    while load_images(c, 10) == 10:
        db.commit()
        time.sleep(5)
        print('Load more images...')

    db.commit()
    db.close()


def main():
    perform_tasks()


if __name__ == '__main__':
    main()
