from os import path


def download_page(url: str, referer: str=None) -> str:
    import urllib.request
    req = urllib.request.Request(url)
    if referer is not None:
        req.add_header('Referer', referer)
    ret = urllib.request.urlopen(req)
    return ret.read()


def download_binary(url: str, file: str, referer: str=None) -> str:
    ret = download_page(url, referer)

    name, ext = path.splitext(url)
    file_with_ext = file + ext
    with open(file_with_ext, 'wb') as f:
        f.write(ret)
    return file_with_ext
