import urllib.parse
import network_helper as nh
from bs4 import BeautifulSoup


TAG_PATH = 'http://joyreactor.cc/tag/%s/'
SEARCH_PATH = 'http://joyreactor.cc/search?q=%s'
POST_PATH = 'http://joyreactor.cc/post/%d'


def parse_page(page: str):
    page_data = {'posts': []}
    soup = BeautifulSoup(page)
    for post in soup.findAll('div', {'class': 'postContainer'}):
        id = int(post['id'][len('postContainer'):])
        post_data = {'id': 'joy%d' % id, 'post': POST_PATH % id}

        taglist = post.find('h2', {'class': 'taglist'})
        post_data['tags'] = [a.string for a in taglist.findAll('a')]

        content = post.find('div', {'class': 'post_content'})
        if content is None:
            continue

        images = content.findAll('div', {'class': 'image'})
        post_images = []
        for div in images:
            gif = div.find('a', {'class', 'video_gif_source'})
            img = div.find('img')
            if gif is not None:
                post_images.append(gif['href'])
            elif img is not None:
                post_images.append(img['src'])

        post_data['images'] = post_images
        page_data['posts'].append(post_data)

    pagination = soup.find('div', {'class': 'pagination_main'})
    prev = pagination.find('a', {'class': 'prev'})
    if prev is not None:
        next_page = prev['href']
        idx = next_page.rfind('/')
        if idx != -1:
            index_str = next_page[idx + 1:]
            if index_str.isdigit():
                index = int(index_str)
                page_data['next'] = index

    return page_data


def download_tag(tag: str, page: int):
    url = TAG_PATH % urllib.parse.quote_plus(tag)
    if page is not None:
        url += str(page)

    html = nh.download_page(url)
    return parse_page(html)


def download_search(search: str):
    url = SEARCH_PATH % urllib.urlencode(search)
    html = nh.download_page(url)
    return parse_page(html)
