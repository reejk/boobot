import random
import sqlite3
import time
import math

DB_NAME = 'boobot.db'
LOCAL_PATH = '/var/www/local.wnezros.com/boobot/'
WEB_PATH = 'http://local.wnezros.com/boobot/'


class Image:
    def __init__(self, t):
        self.image = t[0]
        self.post = t[1]
        self.file = t[2]
        self.width = t[3]
        self.height = t[4]

    def set_sent(self, id: int):
        db = sqlite3.connect(DB_NAME)
        c = db.cursor()
        now = int(time.time())
        c.execute('UPDATE data SET last_id=?,last_seen=? WHERE image=?', (id, now, self.image))
        db.commit()
        db.close()

    def get_url(self):
        return WEB_PATH + self.file[len(LOCAL_PATH):]


def find_by_tag(tag: str) -> Image:
    tag_like = '%%~%s~%%' % tag
    db = sqlite3.connect(DB_NAME)
    c = db.cursor()
    c.execute('SELECT COUNT(image) FROM data WHERE tags LIKE ?', (tag_like,))
    count = c.fetchone()[0]
    offset = int(count * math.log(1 + random.random()))
    print('Selected ', tag, 'is', offset, 'of', count)
    c.execute('SELECT image,post,file,width,height FROM data WHERE tags LIKE ? ORDER BY last_seen ASC LIMIT 1 OFFSET ?',
              (tag_like, offset,))
    res = c.fetchone()
    db.close()
    if res is None:
        return None

    return Image(res)


def find_random() -> Image:
    db = sqlite3.connect(DB_NAME)
    c = db.cursor()
    c.execute('SELECT COUNT(image) FROM data')
    count = c.fetchone()[0]
    offset = int(count * math.log(1 + random.random()))
    c.execute('SELECT image,post,file,width,height FROM data ORDER BY last_seen ASC LIMIT 1 OFFSET ?', (offset,))
    res = c.fetchone()
    db.close()
    if res is None:
        return None

    return Image(res)


def find_by_id(id: int) -> Image:
    db = sqlite3.connect(DB_NAME)
    c = db.cursor()
    c.execute('SELECT image,post,file,width,height FROM data WHERE last_id=?', (id,))
    res = c.fetchone()
    db.close()
    if res is None:
        return None

    return Image(res)


def main():
    print(find_by_tag('сиськи').file)


if __name__ == '__main__':
    main()