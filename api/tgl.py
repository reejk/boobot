import datetime

from typing import Callable


# Callbacks API


def set_on_binlog_replay_end(on_binlog_replay_end):
    pass


def set_on_get_difference_end(on_get_difference_end):
    pass


def set_on_our_id(on_our_id):
    pass


def set_on_msg_receive(on_msg_receive):
    pass


def set_on_secret_chat_update(on_secret_chat_update):
    pass


def set_on_user_update(on_user_update):
    pass


def set_on_chat_update(on_chat_update):
    pass


def set_on_loop(on_loop):
    pass

# Peers api

PEER_CHAT = 0
PEER_USER = 0
PEER_ENCR_CHAT = 0


class Peer:
    id = 0  #
    type = -1  #
    type_name = ''  #
    name = 'example'  #

    def send_typing(self):
        """Tell peer that you are typing."""
        print('typing...')


    def send_typing_abort(self):
        """Tell peer you are done typing."""
        print('not typing...')


    def send_msg(self, text: str, cb: msg_cb=None, reply: int=0, preview: bool=True):
        """Sends message to peer.
        :param text: message
        :param reply_message_id: is the message id we are replying to
        :param preview: is a boolean that forces URL preview on or off
        :param msg_cb: """
        print('Sending message', '(reply to', reply, ', preview:', preview, '):')
        print('<<<')
        print(text)
        print('>>>')


    def fwd_msg(self, msg_id: int, cb: msg_cb=None):
        """Forwards msg with message id to peer."""
        print('Forward message', msg_id)


    def fwd_media(self, msg_id: int, cb: msg_cb=None):
        """Forwards media with message id to peer."""
        print('Forward media', msg_id)


    def send_photo(self, file: str, cb: msg_cb=None):
        """Send media to peer using file. No checking is done on the contents of the file."""
        print('Send photo', file)


    def send_video(self, file: str, cb: msg_cb=None):
        """Send media to peer using file. No checking is done on the contents of the file."""
        print('Send video', file)


    def send_audio(self, file:str, cb: msg_cb=None):
        """Send media to peer using file. No checking is done on the contents of the file."""
        print('Send audio', file)


    def send_document(self, file: str, cb: msg_cb=None):
        """Send media to peer using file. No checking is done on the contents of the file."""
        print('Send document', file)


    def send_text(self, file: str, cb: msg_cb=None):
        """Send media to peer using file. No checking is done on the contents of the file."""
        print('Send text', file)


    def send_location(self, latitude: float, longitude: float, cb: msg_cb=None):
        """Sends location media message to peer, longitude and latitude should be specified as double."""
        print('Send location', latitude, longitude)


    def mark_read(self, cb: empty_cb):
        """Marks the dialog with the peer as read. This cannot be done on message level."""
        print('Mark read')


    def msg_search(self, text: str, cb: msg_list_cb):
        """Get all messages that match the search text with the peer."""
        pass


    def info(self, cb: peer_cb):
        """Gets peer info."""
        pass


class PeerChat(Peer):  # 'virtual' class
    type = PEER_CHAT
    type_name = 'chat'
    user_list = []  # peer_list, not works

    def chat_add_user(self, user: PeerUser):
        """Adds user(tgl.Peer) to the group."""
        print('Add user to chat', user)


    def chat_del_user(self, user: PeerUser):
        """Removes user(tgl.Peer) from the group."""
        print('Delete user from chat', user)


    def rename_chat(self, new_name: str, cb: empty_cb):
        pass


    def chat_set_photo(self, file: str, cb: msg_cb):
        """Sets avatar for the group to image found at file_path, no checking on the file.
        The calling peer must be of type tgl.PEER_CHAT."""
        pass


class PeerUser(Peer):  # 'virtual' class
    type = PEER_USER
    type_name = 'user'
    user_status = {'online': False, 'when': datetime.datetime.now()}
    phone = '+700000'
    username = 'nick'
    first_name = 'name'
    last_name = 'name'


class PeerSecretChat(Peer):  # 'virtual' class
    type = PEER_ENCR_CHAT
    type_name = 'secret_chat'
    user_id = 0


# Message API

ACTION_NONE = 0
ACTION_GEO_CHAT_CREATE = 0
ACTION_GEO_CHAT_CHECKIN = 0
ACTION_CHAT_CREATE = 0
ACTION_CHAT_EDIT_TITLE = 0
ACTION_CHAT_EDIT_PHOTO = 0
ACTION_CHAT_DELETE_PHOTO = 0
ACTION_CHAT_ADD_USER = 0
ACTION_CHAT_ADD_USER_BY_LINK = 0
ACTION_CHAT_DELETE_USER = 0
ACTION_SET_MESSAGE_TTL = 0
ACTION_READ_MESSAGES = 0
ACTION_DELETE_MESSAGES = 0
ACTION_SCREENSHOT_MESSAGES = 0
ACTION_FLUSH_HISTORY = 0
ACTION_RESEND = 0
ACTION_NOTIFY_LAYER = 0
ACTION_TYPING = 0
ACTION_NOOP = 0
ACTION_COMMIT_KEY = 0
ACTION_ABORT_KEY = 0
ACTION_REQUEST_KEY = 0
ACTION_ACCEPT_KEY = 0


class Msg:
    id = 0  # Message id
    flags = 0  # tgl flags, see source code for tgl for various possible flags. This is a bitfield in an int.
    mention = False  # True if you are @mentioned.
    out = False  # True if you sent this message.
    unread = False  # True if you have not marked this as read.
    service = False  # True if the message is a service messages, see tgl.Msg.action for the type.
    src = Peer()  # Peer who sent the message
    dest = Peer()  # Peer who the message was sent too. In a group, this will be the chat peer. Otherwise it will be you.
    text = ''  # Text contents of the message. This may be None if it's media without caption.
    media = {}  # Dictionary that varies based on the media type.
    date = datetime.datetime.now()  # When the message was sent.
    fwd_src = Peer()  # The user that sent the forwarded message.
    fwd_date = datetime.datetime.now()  # When the forwarded message was originally sent.
    reply = None  # Message that this message is replying to.
    reply_id = 0  # Message id that this message is replying to.
    action = 0  # Action enum for the message if Msg.service == True. See all the possible values below

    def load_photo(self, cb: file_cb):
        """Saves the media and returns the path to the file in the callback.:"""
        pass


    def load_video(self, cb: file_cb):
        """Saves the media and returns the path to the file in the callback."""
        pass


    def load_video_thumb(self, cb: file_cb):
        """Saves the media and returns the path to the file in the callback."""
        pass


    def load_audio(self, cb: file_cb):
        """Saves the media and returns the path to the file in the callback."""
        pass


    def load_document(self, cb: file_cb):
        """Saves the media and returns the path to the file in the callback."""
        pass


    def load_document_thumb(self, cb: file_cb):
        """Saves the media and returns the path to the file in the callback."""


    def delete_msg(self):
        pass


# Callbacks

empty_cb = Callable[[bool], any]
contact_list_cb = Callable[[bool, [Peer]], any]
dialog_list_cb = Callable[[bool, {'peer': Peer, 'message': Msg}], any]
msg_cb = Callable[[bool, Msg], any]
msg_list_cb = Callable[[bool, [Msg]], any]
file_cb = Callable[[bool, str], any]
chat_cb = Callable[[bool, PeerChat], any]
peer_cb = Callable[[bool, Peer], any]
secret_chat_cb = Callable[[bool, PeerSecretChat], any]
user_cb = Callable[[bool, PeerUser], any]
str_cb = Callable[[bool, str], any]

# API

def get_contact_list(cb: contact_list_cb):
    """Retrieve peers stored in the contact list. requires callback"""
    pass


def get_dialog_list(cb: dialog_list_cb):
    """Get a list of current conversations with the tgl.Peer and the most recent tgl.Msg. requires callback"""
    pass


def add_contact(phone: str, first_name: str=None, last_name: str=None, cb: contact_list_cb=None):
    """Adds contact to contact list, last name is optional"""
    pass


def del_contact(peer: PeerUser, cb: empty_cb=None):
    """Removes contact from contact list"""
    pass


def rename_contact(phone: str, first_name: str, last_name: str, cb: contact_list_cb=None):
    """Renaming a contact means sending the first/last name with the same phone number of an existing contact"""
    pass


def msg_global_search(text: str, cb: msg_list_cb):
    """Get all messages that match the search text."""
    pass


def set_profile_photo(file_path: str, cb: empty_cb=None):
    """Sets avatar to image found at file_path, no checking on the file."""
    pass


def create_secret_chat(user: PeerUser, cb: secret_chat_cb=None):
    """Creates secret chat with user, callback recommended to get new peer for the secret chat."""
    pass


def create_group_chat(peer_list: [Peer], name: str, cb: empty_cb=None):
    """peer_list contains users to create group with, must be more than 1 peer."""
    pass


def restore_msg(msg_id: int, cb: empty_cb=None):
    """Restore a deleted message by message id."""
    pass


def status_online(cb: empty_cb=None):
    """Sets status as online"""
    pass


def status_offline(cb: empty_cb=None):
    """Sets status as offline """
    pass


def import_chat_link(link: str, cb: empty_cb=None):
    """Join channel using the link."""
    pass


def safe_exit(exit_status: int=0):
    """Exception to the rule about callbacks, no callback parameter is accepted.
    Causes the bot to quit after cleaning up.
    exit_status is an optional parameter with the exit status (On glibc, this must be 0-255)"""
    pass


def get_history(peer: Peer, offset: int, limit: int, cb: msg_list_cb):
    """Get all messages with the peer. offset See example below for one method to get the entire history.
    :param offset: specifies what message to start at
    :param limit: specifies how many messages to retrieve.
    """
    pass
